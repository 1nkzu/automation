import javafx.util.Pair;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login {

    @FindBy (xpath = "//*[@id=\"login-front\"]/div/div/div/div/div/form/div[2]/input")
    public WebElement password;

    @FindBy (xpath = "//*[@id=\"login-front\"]/div/div/div/div/div/form/div[1]/input")
    public WebElement user;

    public void Login(WebDriver driver, String userEmail, String userPassword) {
        driver.findElement(By.xpath("//*[@id=\"main-menu\"]/ul/li[5]/a")).click();
        driver.findElement(By.xpath("//*[@id=\"login-front\"]/div/div/div/div/div/h3")).isDisplayed();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"login-front\"]/div/div/div/div/div/form/div[1]/input")));
        user.clear();
        user.sendKeys(userEmail);
        password.clear();
        password.sendKeys(userPassword);
        driver.findElement(By.xpath("//*[@id=\"login-front\"]/div/div/div/div/div/form/div[4]/div/div/button")).click();
    }

}