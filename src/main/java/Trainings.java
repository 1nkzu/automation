import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class Trainings {
    private WebDriver driver;

    public List<WebElement> allTrainings(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Vaata kõiki trenne")));
        driver.findElement(By.linkText("Vaata kõiki trenne")).click();
        List<WebElement> addedTrainings = driver.findElements(By.className("workout-item"));
        System.out.println("Currently added trainings are : ");
        for(WebElement addedTraining : addedTrainings){
            System.out.println(addedTraining.getText());
        }

        return addedTrainings;
    }

    public WebElement addTraining(WebDriver driver, String baseUrl, HashMap<String, String> training) throws InterruptedException {
        WebElement chosenExercise = null;

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("a[href=\"/staging/workout\"]")));

        System.out.println("Searching for button 'Uus trenn'.");
        // More than one element has this same link, so we have to find the one that is displayed
        List<WebElement> elements = driver.findElements(By.cssSelector("a[href=\"/staging/workout\"]"));
        for(WebElement element : elements){
            if(element.isDisplayed()){
                element.click();
            }
        }

        System.out.println("Filling in workout form...");

        // Filling workout form
        driver.findElement(By.id("workout_display_date")).click();
        HandleJQueryDateTimePicker(training.get("Date"), driver);
        System.out.println("Date chosen, trying to get the right exercise...");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"workout_form\"]/div[1]/div[3]/div[1]/div[1]/input")));
        driver.findElement(By.xpath("//*[@id=\"workout_form\"]/div[1]/div[3]/div[1]/div[1]/input")).sendKeys(training.get("Exercise").substring(0,2));
        System.out.println("Exercise name beginning typed...");

        // Could not get the correct solution, only one word data can be used right now.
        String xpath_exercise = "//li[@class='ui-menu-item']/a[contains(@id, 'ui-id-')][normalize-space()= '" + training.get("Exercise").trim() + "']";


        System.out.println("Otsin treeningut: " + training.get("Exercise"));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath_exercise)));
        WebElement optionToSelect = driver.findElement(By.xpath(xpath_exercise));
        optionToSelect.click();
        System.out.println("Yesss, option chosen!");

        driver.findElement(By.cssSelector("input[class=\"exercise_weight form-control\"]")).sendKeys(training.get("Weight"));
        driver.findElement(By.cssSelector("input[class=\"exercise_reps form-control\"]")).sendKeys(training.get("Reps"));
        driver.findElement(By.id("bodyweight")).sendKeys(training.get("Bodyweight"));
        driver.findElement(By.id("notes")).sendKeys(training.get("Notes"));
        driver.findElement(By.xpath("//*[@id=\"workout_form\"]/div[5]/div/div[1]/button")).click();
        System.out.println("Training added.");
        driver.navigate().to(baseUrl);

        return chosenExercise;
    }

    // allikas: https://www.techbeamers.com/handle-date-time-picker-control-using-webdriver/
    public void HandleJQueryDateTimePicker(String day, WebDriver driver) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ui-datepicker-div")));
        WebElement table = driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table"));
        System.out.println("JQuery Calendar Dates");

        List<WebElement> tableRows = table.findElements(By.xpath("//tr"));
        for (WebElement row : tableRows) {
            List<WebElement> cells = row.findElements(By.xpath("td"));

            for (WebElement cell : cells) {
                if (cell.getText().equals(day)) {
                    driver.findElement(By.linkText(day)).click();
                }
            }
        }

        // Switch back to the default screen again
        driver.switchTo().defaultContent();
        ((JavascriptExecutor) driver).executeScript("scroll(0, -250);");

        // Intentional pause for 2 seconds.
        Thread.sleep(2000);

    }

    public void deleteTraining(WebDriver driver, HashMap<String, String> training){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Vaata kõiki trenne")));
        driver.findElement(By.linkText("Vaata kõiki trenne")).click();
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        String yearInString = String.valueOf(year);
        String date = getMonth() + " " + training.get("Date") + ", " + yearInString;
        System.out.println("Searching for trainings with a date : " + date);

        int elements = driver.findElements(By.cssSelector("a[data-date='" + date + "']")).size();
        System.out.println("Number of such trainings: " + elements);

        for (int i = 0; i < elements; i++) {
            List<WebElement> thisDateTrainings = driver.findElements(By.cssSelector("a[data-date='" + date + "']"));
            WebElement client = thisDateTrainings.get(i);
            client.click();
            String exercise = driver.findElement(By.xpath("//*[@id=\"workout_form\"]/div[1]/div[3]/div[1]/div[1]/input")).getAttribute("value");
            String weight = driver.findElement(By.cssSelector("input[class=\"exercise_weight form-control\"]")).getAttribute("value");
            String reps = driver.findElement(By.cssSelector("input[class=\"exercise_reps form-control\"]")).getAttribute("value");
            String bodyweight = driver.findElement(By.id("bodyweight")).getAttribute("value");
            String notes = driver.findElement(By.id("notes")).getAttribute("value");

            System.out.println("Searching for a training that has previously added parameters...");
            if(exercise.equals(training.get("Exercise")) && weight.equals((training.get("Weight"))) && reps.equals(training.get("Reps")) && bodyweight.equals(training.get("Bodyweight")) && notes.equals(training.get("Notes"))){
                System.out.println("Found it! And now will delete it...");
                driver.findElement(By.cssSelector("a[data-form='.delete-workout']")).click();
                driver.switchTo().alert().accept();
                break;
            }
            driver.navigate().back();
        }
    }

    public boolean checkIfAdded(WebDriver driver, String baseUrl, HashMap<String, String> training){
        boolean ifAdded = false;

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Vaata kõiki trenne")));
        driver.findElement(By.linkText("Vaata kõiki trenne")).click();
        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        String yearInString = String.valueOf(year);
        String date = getMonth() + " " + training.get("Date") + ", " + yearInString;
        System.out.println("Searching for trainings with a date : " + date);

        int elements = driver.findElements(By.cssSelector("a[data-date='" + date + "']")).size();
        System.out.println("Number of such trainings: " + elements);

        for (int i = 0; i < elements; i++) {
            List<WebElement> thisDateTrainings = driver.findElements(By.cssSelector("a[data-date='" + date + "']"));
            WebElement client = thisDateTrainings.get(i);
            client.click();
            String exercise = driver.findElement(By.xpath("//*[@id=\"workout_form\"]/div[1]/div[3]/div[1]/div[1]/input")).getAttribute("value");
            String weight = driver.findElement(By.cssSelector("input[class=\"exercise_weight form-control\"]")).getAttribute("value");
            String reps = driver.findElement(By.cssSelector("input[class=\"exercise_reps form-control\"]")).getAttribute("value");
            String bodyweight = driver.findElement(By.id("bodyweight")).getAttribute("value");
            String notes = driver.findElement(By.id("notes")).getAttribute("value");

            System.out.println("Searching for a training that has previously added parameters...");
            if(exercise.equals(training.get("Exercise")) && weight.equals((training.get("Weight"))) && reps.equals(training.get("Reps")) && bodyweight.equals(training.get("Bodyweight")) && notes.equals(training.get("Notes"))){
                System.out.println("Found it!");
                ifAdded = true;
                break;
            }
        }
        driver.navigate().to(baseUrl);
        return ifAdded;
    }

    public String getMonth() {
        String[] monthName = {"January", "February",
                    "March", "April", "May", "June", "July",
                    "August", "September", "October", "November",
                    "December"};

        Calendar cal = Calendar.getInstance();
        String month = monthName[cal.get(Calendar.MONTH)];

        return month;
    }





}