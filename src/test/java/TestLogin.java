import javafx.util.Pair;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Set;


public class TestLogin {
    private WebDriver driver;
    private Pair<String, String> correctUser = new Pair<>("ingakonovalova@gmail.com", "jHb1YcCA");
    private HashMap<String, String> incorrectUsers  = new HashMap<String, String>() {{
        put("mesikäpp@gmail.com", "jHb1YcCA");
        put("ingakonovalova@gmail.com", "maasikas");
        put("", "");
        put("", "teddybear1");
        put("trolling@gmail.com", "");
    }};
    private String baseUrl = "https://www.gymwolf.com/staging/";

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "D:\\Desktop\\Projects\\lib\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to(baseUrl);
    }
    @After
    public void teardown(){
        driver.close();
    }

    @Test
    public void CorrectLogin(){
        Login L = PageFactory.initElements(driver, Login.class); //
        driver.get(baseUrl);

        System.out.println("Starting to log in...");
        L.Login(driver, correctUser.getKey(), correctUser.getValue());

        // Checking if logged in user is actually logged in and profile shows correct email.
        driver.findElement(By.xpath("//*[@id=\"main-menu\"]/ul/li[6]/a")).click();
        driver.findElement(By.linkText("Muuda profiili")).click();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"tabs-1\"]/div[2]/div[1]/div/p")));
        String email = driver.findElement(By.xpath("//*[@id=\"tabs-1\"]/div[2]/div[1]/div/p")).getText();
        System.out.println("User " + email + " has been logged in.");
        Assert.assertEquals(correctUser.getKey(), email);
    }

    @Test
    public void IncorrectLogins(){
        Set keys = incorrectUsers.keySet();
        Login L = PageFactory.initElements(driver, Login.class);
        for(Object key : keys) {
            driver.get(baseUrl);
            L.Login(driver, key.toString(), incorrectUsers.get(key).toString());
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div")));
            System.out.println("Waiting for alert...");
            WebElement element = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div"));
            System.out.println("Alert is present.");
            Assert.assertEquals(true, element.isDisplayed());
        }
    }
}
