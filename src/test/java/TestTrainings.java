import javafx.util.Pair;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import java.util.HashMap;
import java.util.List;

public class TestTrainings {
    private WebDriver driver;
    // Needs more data for testing
    private Pair<String, String> user = new Pair<>("ingakonovalova@gmail.com", "jHb1YcCA");
    private HashMap<String, String> training  = new HashMap<String, String>() {{
        put("Date", "18");
        put("Exercise", "Kõhumasin");
        put("Weight", "25");
        put("Reps", "15");
        put("Bodyweight", "67");
        put("Notes", "My best training");
    }};
    private String baseUrl = "https://www.gymwolf.com/staging/";
    private int numberOfTrainings;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "D:\\Desktop\\Projects\\lib\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-popup-blocking");
        driver = new ChromeDriver(options);
        Login L = PageFactory.initElements(driver, Login.class); //
        driver.get(baseUrl);
        System.out.println("Starting to log in...");
        L.Login(driver, user.getKey(), user.getValue());

        System.out.println("Getting number of already added trainings...");
        Trainings T = PageFactory.initElements(driver, Trainings.class);
        List<WebElement> addedTrainings = T.allTrainings(driver);
        numberOfTrainings = addedTrainings.size();
        driver.navigate().back();
    }

    @After
    public void teardown(){
        driver.close();
    }

    @Test
    public void addGymTrainingToTheList() throws InterruptedException {
        System.out.println("Number of already added trainings is: " + numberOfTrainings);

        System.out.println("Starting adding new workout...");
        Trainings T = PageFactory.initElements(driver, Trainings.class);
        T.addTraining(driver, baseUrl, training);

        System.out.println("Calculating new number of added trainings...");
        List<WebElement> addedTrainings = T.allTrainings(driver);

        int numberAfterAdding = addedTrainings.size();
        System.out.println("After adding the number of trainings is: " + numberAfterAdding);

        Assert.assertEquals(numberAfterAdding, numberOfTrainings+1);
        System.out.println("Checked if list has +1 elements this time.");
        driver.navigate().to(baseUrl);

        System.out.println("Now must delete previously added training...");
        T.deleteTraining(driver, training);
    }

    @Test
    public void checkAddedGymTraining() throws InterruptedException {
        System.out.println("Starting adding new workout...");
        Trainings T = PageFactory.initElements(driver, Trainings.class);
        T.addTraining(driver, baseUrl, training);

        System.out.println("Check if added workout data is in list of trainings.");
        boolean ifAdded = T.checkIfAdded(driver, baseUrl, training);
        Assert.assertEquals(true, ifAdded);

        System.out.println("Now must delete previously added training...");
        T.deleteTraining(driver, training);
    }

    @Test
    public void deleteGymTrainingFromList() throws InterruptedException{
        System.out.println("Number of already added trainings is: " + numberOfTrainings);

        System.out.println("Starting adding new workout that will be deleted later.");
        Trainings T = PageFactory.initElements(driver, Trainings.class);
        T.addTraining(driver, baseUrl, training);

        int numberAfterAdding = T.allTrainings(driver).size();
        driver.navigate().to(baseUrl);
        System.out.println("Number of trainings after adding is: " + numberAfterAdding);

        System.out.println("Starting to delete added training...");
        T.deleteTraining(driver, training);

        int numberAfterRemoving = T.allTrainings(driver).size();
        System.out.println("After removing the number of trainings is: " + numberAfterRemoving);

        System.out.println("Checked if after adding list has -1 elements this time.");
        Assert.assertEquals(numberAfterAdding-1, numberAfterRemoving);
    }

}
